# `git(lab)` tests

*Trying out services, functionalities, workflows, whatever.*

### Create a gitlab repo from existing local.
Creat blank repo (here at breyne/tests) then 
```bash
git remote rename origin old-origin # if exists an other one
git remote add origin https://gitlab.com/breyne/tests.git
git push -u origin master --all 
git push -u origin mester --tags
```

### Test tree behavior with branches dev, trash, feature.

A feautre-based workflow with minimal log in `master`, 
complete log in `dev`.
```bash
git co dev #
git co -b feat_01 # new feat branch tracking dev

# feat developments from feat_01
# commits from feat_01

git rebase dev # organise detached commits
git co dev
git merge feat_01

git co master
git merge --squash  dev # not actually commiting, just preparing the next commit
git commit #only one commit instead of the whole thread
```

### Juggle with several remotes

With this we create a development branch that tracks another remote
```bash
git remote add other https://gitlab.com/breyne/other-tests.git
git checkout -b dev
#git br --set-upstream-to=other/master
git push -u other dev --all 
git push -u other dev --tags
```


### Gitlab-pages

### Markdown

Quick try:


Consider a single crystal with a set of slip systems $(\mathbf M_\alpha)_{1\leq\alpha\leq N}$ where 
\begin{align}
 \mathbf M_\alpha = \mathbf s_a\otimes \mathbf n_b,
\end{align}
$$
 \mathbf M_\alpha = \mathbf s_a\otimes \mathbf n_b,
$$
that is the outer product of a slip direction $\mathbf s_a$ and a slip plane normal $\mathbf n_b$
